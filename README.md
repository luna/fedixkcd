# fedixkcd

relevant-xkcd bot for fedi

## note

i'm mixing asyncio (aiohttp, websockets) with non-async code (mastodon.py)
purely out of laziness. i could use `run_in_executor` and the likes to
prevent the blocking nature of mastodon.py, but i'm lazy.

## runtime requirements

 - python 3.7
 - pipenv
 - [yuki](https://gitlab.com/luna/yuki) for xkcd searching

## doing the do

```bash
pipenv install
cp config.example.ini config.ini

# edit as wanted
$EDITOR config.ini

# runningmn... colonel...
pipenv run python bot.py
```
