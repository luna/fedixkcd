# fedixkcd
# Copyright 2019, Luna Mendes and fedixkcd contributors
# SPDX-License-Identifier: GPL-3.0-only

import re
import json
import logging
import asyncio
import mimetypes
from typing import Union, Callable

from pathlib import Path
from configparser import ConfigParser

import websockets
import mastodon
from aiohttp import ClientSession
from bs4 import BeautifulSoup, Tag

log = logging.getLogger(__name__)


def _tag_replace(soup, selector: str, 
                 replace_with: Union[str, Callable[[Tag], str]],
                 *, unwrap: bool = False):
    for tag in soup.select(selector):
        if callable(replace_with):
            tag.insert_after(replace_with(tag))
        else:
            tag.insert_after(replace_with)

        if unwrap:
            tag.unwrap()
        else:
            tag.decompose()


def _tag_unwrap(soup, selector: str):
    for tag in soup.select(selector):
        tag.unwrap()


def sanitize_content(string: str) -> str:
    """Sanitizer for the HTML content in posts.

    Modified from https://github.com/Lynnesbian/OCRbot/blob/master/reply.py#L37
    """

    # replace html tags
    string = string.replace('&apos;', "'")
    string = string.replace('&quot;', '"')

    soup = BeautifulSoup(string, 'html.parser')

    # replace <br> tags with a \n
    _tag_replace(soup, 'br', '\n')
    _tag_replace(soup, 'p', '\n', unwrap=True)

    # unwrap hashtags to just their containing text
    _tag_unwrap(soup, 'a.hashtag')

    # rewrite <a> tags to just their href content instead of all the included
    # properties
    _tag_replace(soup, 'a', lambda tag: tag['href'])

    string = soup.get_text()

    # re-inject mentions back (for mastodon and pleroma respectively)
    string = re.sub("https://([^/]+)/(@[^ ]+)", r"\2@\1", string)
    string = re.sub("https://([^/]+)/users/([^ ]+)", r"@\2@\1", string)

    string = string.rstrip('\n')
    # text = re.sub(r"^@[^@]+@[^ ]+\s*", r"", text) #remove the initial mention

    return string.lower()


async def media_post(client, image_url: str) -> dict:
    """GETs the given image url and returns a media object."""
    mimetype, _ = mimetypes.guess_type(image_url)

    async with client.aiohttp_session.get(image_url) as resp:
        img_bytes = await resp.read()

    log.debug('getting and posting %r (%d bytes)',
              image_url, len(img_bytes))

    return client.media_post(img_bytes, mimetype)


async def handle_notif(payload, client):
    """Process a notification.

    Extracts the actual post's text via sanitize_content() and passes it on
    to a yuki instance, which then does the searching through its xkcd data
    and responds back with a list of comics.

    Ignores notifications that aren't mentions.
    """

    if payload['type'] != 'mention':
        return

    acct = f'@{payload["account"]["acct"]}'
    post = payload['status']
    log.info('processing mention from %r', acct)

    in_reply_to = client.status(payload['status']['in_reply_to_id'])

    if in_reply_to is None:
        log.warning('post being replied from not found')
        return

    self_user = client.cfg['user']
    if in_reply_to['account']['acct'] == f'@{self_user}':
        log.warning('post being replied from is ourselves, ignoring')
        return

    words = sanitize_content(in_reply_to['content']).split()

    # for now, we use the longest word in the post to send to yuki.
    # because yuki (and relevant-xkcd for that matter) handle multi-word
    # search parameters. they get cut off since the first one.
    main_word = sorted(words, key=len, reverse=True)[0]

    yuki_url = client.cfg['yuki_url']

    async with client.aiohttp_session.post(
        yuki_url, json={'search': main_word}) as resp:
        try:
            data = await resp.json()
        except Exception:
            body = (await resp.text())[:100]
            log.error('failed to contact yuki: %r...', body)
            return

    if not data['success']:
        log.error('failed to contact yuki')
        return

    log.info('%d comics found', len(data['results']))

    if not data['results']:
        res = 'No comics found.'
        comic = None
    else:
        comic = data['results'][0]
        res = (f'{acct} comic found: '
               f'{comic["number"]} {comic["title"]!r} {comic["url"]}')

    try:
        if comic:
            media = await media_post(client, comic['image_url'])
        else:
            media = None
    except mastodon.MastodonAPIError:
        log.exception('error while uploading media')
        media = None

    kwargs = {}

    if media:
        kwargs['media_ids'] = [media['id']]

    post = client.status_post(
        res, post['id'], visibility=post['visibility'],
        **kwargs
    )

    log.info('reply success! %s %r', post['id'], post['url'])


async def streaming_loop(client, conn):
    """main streaming loop.

    dispatches incoming notification events to handle_notif().
    """
    while True:
        msg = await conn.recv()

        if not msg:
            continue

        msg = json.loads(msg)
        event = msg['event']

        try:
            payload = json.loads(msg['payload'])
        except json.JSONDecodeError:
            continue

        if event != 'notification':
            continue

        try:
            await handle_notif(payload, client)
        except:
            log.exception('error while handling notification')


async def start_websockets(client):
    """start websockets for streaming.

    This does not use Mastodon.py's streaming routines, instead we use
    the websockets library and do everything raw-ly.

    This was because Mastodon.py does not work well with Pleroma MastoAPI's
    streaming URLs. It assumes its going to be SSE (server sent events) but
    it's not and so it breaks hard. Pleroma's MastoFE does use the websocket
    approach so... I don't know who to blame here.

    Anyways, it works.
    """
    # inject client session because aiohttp only likes doing it inside
    # a coroutine
    client.aiohttp_session = ClientSession()

    instance = client.instance()
    token = client.access_token
    url = (f'{instance["urls"]["streaming_api"]}/api/v1/streaming'
           f'/?stream=user&access_token={token}')

    log.debug('websocket url %r', url)
    conn = await websockets.connect(url)

    try:
        log.info('websocket connected')
        await streaming_loop(client, conn)
    except:
        log.exception('error on streaming loop')
    finally:
        await conn.close()


def main():
    """Main entrypoint"""
    cfg = ConfigParser()
    cfg.read('./config.ini')

    cwd = Path.cwd()
    bot_cfg = cfg['fedixkcd']

    if bot_cfg['debug']:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    usercred_path = cwd / 'fedixkcd_appcred.secret'

    if not usercred_path.exists():
        log.info('usercred.secret does not exist, creating')

        mastodon.Mastodon.create_app(
            'fedixkcd',
            api_base_url=bot_cfg['instance'],
            to_file=usercred_path
        )

    client = mastodon.Mastodon(
        api_base_url=bot_cfg['instance'],
        client_id='fedixkcd_appcred.secret'
    )

    # inject config into the client
    client.cfg = bot_cfg

    log.info('logging in')
    client.log_in(
        bot_cfg['user'],
        bot_cfg['password'],
        to_file='fedixkcd_usercred.secret'
    )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_websockets(client))


if __name__ == '__main__':
    try:
        main()
    except:
        log.exception('error while running')
